﻿using Prism.Events;

namespace GuardiCoreApp.Events
{
    public class DisplaySiteContentEvent : PubSubEvent<string>
    {
    }
}
