
using GuardiCoreApp.Interfaces;
using Prism.Events;
using System;
using Unity;
using Unity.Lifetime;

namespace GuardiCoreApp.ViewModel
{
    public class ViewModelLocator : IDisposable
    {
        private IUnityContainer _container;

        public ViewModelLocator()
        {
            _container = new UnityContainer();
            Activate();
        }

        public MainViewModel MainViewModel
        {
            get => _container.Resolve<MainViewModel>();
        }

        public void Activate()
        {
            _container.RegisterType(
                typeof(IEventAggregator),
                typeof(EventAggregator),
                new ContainerControlledLifetimeManager());
            _container.RegisterType(
                typeof(IManageModeViewModel),
                typeof(ManageModeViewModel),
                new ContainerControlledLifetimeManager());
            _container.RegisterType(
                typeof(ISiteContentViewModel),
                typeof(SiteContentViewModel),
                new ContainerControlledLifetimeManager());
            _container.RegisterSingleton<MainViewModel>();

        }

        public void Deactivate()
        {
            ResolveAndDispose<IManageModeViewModel>();
            ResolveAndDispose<ISiteContentViewModel>();
        }

        public void Dispose()
        {
            Deactivate();
        }
        private void ResolveAndDispose<T>()
        {
            var instance = _container.Resolve<T>();
            var disposableInstance = instance as IDisposable;
            disposableInstance?.Dispose();
        }
    }
}