﻿using System;
using System.Collections.Generic;

namespace GuardiCoreApp.Model
{
    public class Item
    {
        public string BasePath { get; set; }

        public string AbsolutePath => $"{BasePath}/{Name}";
        public string Name { get; set; }
        public long Size { get; set; }
        public string SizeString { get; set; }
        public DateTime LastModified { get; set; }
        public string LastModifiedString { get; set; }
        public bool IsDirectory { get; set; }
       
        public IEnumerable<Item> Items { get; set; }
    }
}
