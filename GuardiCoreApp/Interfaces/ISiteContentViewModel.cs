﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GuardiCoreApp.Model;

namespace GuardiCoreApp.Interfaces
{
    public interface ISiteContentViewModel
    {
        List<Item> SelectedItems { set; }

        ObservableCollection<Item> ItemsList { get;}
    }
}
