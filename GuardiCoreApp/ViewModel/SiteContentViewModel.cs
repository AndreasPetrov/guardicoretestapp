﻿using GalaSoft.MvvmLight;
using GuardiCoreApp.Events;
using GuardiCoreApp.Interfaces;
using GuardiCoreApp.Model;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using GalaSoft.MvvmLight.Command;
using HtmlAgilityPack;

namespace GuardiCoreApp.ViewModel
{
    public class SiteContentViewModel : ViewModelBase, ISiteContentViewModel, IDisposable
    {
        private readonly IEventAggregator _eventAggregator;
        private ObservableCollection<Item> _itemsList;
        private List<Item> _selectedItems;
        private readonly SynchronizationContext _uiContext;

        private CancellationTokenSource _cancellationTokenSource;
        private ObservableCollection<ExtensionItem> _extensionItemsList;

        public SiteContentViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            MarkedSelectedItemsChangedCommand =
                new RelayCommand<ObservableCollection<object>>(OnMarkedSelectedItemsChanged);
            _eventAggregator.GetEvent<DisplaySiteContentEvent>().Subscribe(async input =>
            {
                await OnDisplayContent(input);
            });

            _eventAggregator.GetEvent<DownloadFileEvent>().Subscribe(async (input) =>
            {
                await OnDownloadFile(input);
            });
            _eventAggregator.GetEvent<StopSearchFilesEvent>().Subscribe(OnStopSearchFiles);
            _uiContext = SynchronizationContext.Current;
        }

        public RelayCommand<ObservableCollection<object>> MarkedSelectedItemsChangedCommand { get; set; }
        private async Task OnDownloadFile(string targetPath)
        {
            if (!SelectedItems.Any() || SelectedItems.All(x=>x.IsDirectory))
            {
                MessageBox.Show("Please select at least one file");
            }
            else
            {
                foreach (var selectedItem in SelectedItems)
                {
                    var uri = new Uri(selectedItem.AbsolutePath);
                    var request = PrepareWebRequest(WebRequestMethods.Http.Get, uri);
                    await DownloadFile(request, string.Concat(targetPath,"/", selectedItem.Name));
                }

                MessageBox.Show("Downloading of the selected files has been completed");
            }
        }

        private async Task DownloadFile(HttpWebRequest request, string fullTargetPath)
        {
            await Task.Run(() =>
            {
                int bytesProcessed = 0;
                try
                {
                    if (request != null)
                    {
                        using (WebResponse response = request.GetResponse())
                        {
                            if (response != null)
                            {
                                using (Stream remoteStream = response.GetResponseStream())
                                {
                                    using (Stream localStream = File.Create(fullTargetPath))
                                    {
                                        byte[] buffer = new byte[1024];
                                        int bytesRead;

                                        do
                                        {
                                            bytesRead = remoteStream.Read(buffer, 0, buffer.Length);
                                            localStream.Write(buffer, 0, bytesRead);
                                            bytesProcessed += bytesRead;
                                        } while (bytesRead > 0);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            });
        }

        private void OnMarkedSelectedItemsChanged(ObservableCollection<object> markedList)
        {
            SelectedItems.Clear();
            if (markedList != null)
            {
                foreach (var element in markedList)
                {
                    var item = element as Item;
                    SelectedItems.Add(item);
                }
            }
        }

        private void OnStopSearchFiles()
        {
            _cancellationTokenSource.Cancel();
        }

        public List<Item> SelectedItems
        {
            get => _selectedItems ?? (_selectedItems = new List<Item>());
            set
            {
                if (Equals(value, _selectedItems))
                    return;
                _selectedItems = value;
                RaisePropertyChanged(nameof(SelectedItems));
            }
        }

        public ObservableCollection<Item> ItemsList
        {
            get => _itemsList ?? (_itemsList = new ObservableCollection<Item>());
            private set
            {
                if (Equals(value, _itemsList))
                    return;
                _itemsList = value;
                RaisePropertyChanged(nameof(ItemsList));
            }
        }
        public ObservableCollection<ExtensionItem> ExtensionItemsList
        {
            get => _extensionItemsList ?? (_extensionItemsList = new ObservableCollection<ExtensionItem>());
            private set
            {
                if (Equals(value, _extensionItemsList))
                    return;
                _extensionItemsList = value;
                RaisePropertyChanged(nameof(ExtensionItemsList));
            }
        }

        private async Task OnDisplayContent(string targetLink)
        {
            try
            {
                ItemsList.Clear();
                _cancellationTokenSource = new CancellationTokenSource();
                await Task.Run(() =>
                {
                    IEnumerable<Item> returnValue = GetFilesStructure(targetLink).ToList();
                }, _cancellationTokenSource.Token);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"DirectoryListing {ex.Message} {ex.StackTrace} {ex.InnerException?.Message}");
            }
        }

        private IEnumerable<Item> GetFilesStructure(string targetLink)
        {
            string pageContent = GetPageContent(targetLink);

            IEnumerable<string> list = !string.IsNullOrEmpty(pageContent)
                ? pageContent.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).Where(x => x.Contains("href"))
                    .Select(x => x.Trim()).ToArray()
                : null;

            foreach (string line in list)
            {
                if(_cancellationTokenSource.Token.IsCancellationRequested)
                    break;
                yield return ParseHtml(targetLink, line);
            }
        }


        private Item ParseHtml(string targetLink, string htmlString)
        {

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlString);

            var columnNodes = doc.DocumentNode.ChildNodes.First().ChildNodes.ToArray();
            var imagesNodeAttributeAltValue = columnNodes?.Where(x => x.InnerHtml.Contains("img")).First().ChildNodes.First().Attributes["alt"].Value;
            if (imagesNodeAttributeAltValue == "[ICO]") return null;
            var linkNode = columnNodes?.Where(x => x.InnerHtml.Contains("href")).First().ChildNodes.First().InnerText;
            string size = columnNodes[3].InnerText;

            string dtLastModified = columnNodes[2].InnerText;

            Item item = new Item
            {
                BasePath = targetLink,
                Name = linkNode,
                SizeString = size,
                LastModifiedString = dtLastModified.Trim(),
                IsDirectory = imagesNodeAttributeAltValue == "[DIR]",

            };
            if (item.IsDirectory)
                item.Items = GetFilesStructure(item.AbsolutePath).ToList();

            Task.Run(() =>
            {
                Dispatcher.CurrentDispatcher.Invoke(() =>
                {
                    _uiContext.Send(x => ItemsList.Add(item), null);
                    if (!item.IsDirectory)
                    {
                        string ext = Path.GetExtension(item.AbsolutePath);
                        if(!string.IsNullOrEmpty(ext) && !ExtensionItemsList.Select(x=>x.Extension).Contains(ext))
                        _uiContext.Send(x => ExtensionItemsList.Add(new ExtensionItem(ext)), null);
                    }
                });
            });

            return item;
        }

        private string GetPageContent(string targetLink)
        {
            string resultString = "";
            var uri = new Uri(targetLink);
            var request = PrepareWebRequest(WebRequestMethods.Http.Get, uri);
            if (request == null) return resultString;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())

                if (response.GetResponseStream() != null)
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        resultString = reader.ReadToEnd();
                    }

            return resultString;
        }

        /// <summary>
        /// Prepares request for the asynchronous interaction with ftp server
        /// </summary>
        /// <returns></returns>
        public HttpWebRequest PrepareWebRequest(string webRequestMethod, Uri uri, bool useBinaryFlag = true, bool passiveModeFlag = true, bool keepAliveFlag = true, long contentLength = 0)
        {
            try
            {
                // Sent Request
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

                // Specify the resuest Type
                request.Method = webRequestMethod;
                request.KeepAlive = keepAliveFlag;
                request.ReadWriteTimeout = 20000;
                request.Proxy = null;
                if (contentLength > 0) request.ContentLength = contentLength;


                return request;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public void Dispose()
        {
            _eventAggregator.GetEvent<DisplaySiteContentEvent>().Unsubscribe(async input =>
            {
                await OnDisplayContent(input);
            });
        }
    }
}
