﻿using Prism.Events;

namespace GuardiCoreApp.Events
{
    public class DownloadFileEvent : PubSubEvent<string>
    {
    }
}
