﻿using GalaSoft.MvvmLight;

namespace GuardiCoreApp.Model
{
    public class ExtensionItem : ViewModelBase
    {
        public string Extension
        {
            get => _extension;
            private set
            {
                if (Equals(value, _extension))
                    return;
                _extension = value;
                RaisePropertyChanged(nameof(Extension));
            }
        }

        public bool IsDisplayed
        {
            get => _isDisplayed;
            set
            {
                if (Equals(value, _isDisplayed))
                    return;
                _isDisplayed = value;
                RaisePropertyChanged(nameof(IsDisplayed));
            }
        }

        private string _extension;
        private bool _isDisplayed = true;

        public ExtensionItem(string extension)
        {
            Extension = extension;
        }
    }
}
