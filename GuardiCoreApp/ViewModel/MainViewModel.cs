using System;
using GalaSoft.MvvmLight;
using GuardiCoreApp.Interfaces;
using Unity;

namespace GuardiCoreApp.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase, IDisposable
    {
        private IManageModeViewModel _manageModeViewModel;
        private ISiteContentViewModel _siteContentViewModel;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IManageModeViewModel manageModeViewModel, ISiteContentViewModel siteContentViewModel)
        {
            ManageModeViewModel = manageModeViewModel;
            SiteContentViewModel = siteContentViewModel;
        }

        public IManageModeViewModel ManageModeViewModel
        {
            get => _manageModeViewModel;
            set
            {
                _manageModeViewModel = value;
                RaisePropertyChanged(nameof(ManageModeViewModel));
            }
        }

        public ISiteContentViewModel SiteContentViewModel
        {
            get => _siteContentViewModel;
            set
            {
                _siteContentViewModel = value;
                RaisePropertyChanged(nameof(SiteContentViewModel));
            }
        }

        public void Dispose()
        {
            
        }
    }
}