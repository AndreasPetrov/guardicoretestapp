﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GuardiCoreApp.Events;
using GuardiCoreApp.Interfaces;
using Prism.Events;

namespace GuardiCoreApp.ViewModel
{
    public class ManageModeViewModel: ViewModelBase, IManageModeViewModel
    {
        private string _targetLink = @"http://ddebs.ubuntu.com/";
        private readonly IEventAggregator _eventAggregator;
        private string _targetFolder;

        #region .ctor

        public ManageModeViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            DisplayContentCommand = new RelayCommand(OnDisplayContent);
            DownloadWebsiteCommand = new RelayCommand(OnDownloadWebsite);
            SelectedPathCommand = new RelayCommand(OnSelectedPathCommand);
            StopSearchFilesCommand = new RelayCommand(OnStopSearchFiles);
        }

        private void OnStopSearchFiles()
        {
            _eventAggregator.GetEvent<StopSearchFilesEvent>().Publish();
        }

        private void OnSelectedPathCommand()
        {
            TargetFolder = GetFolderPath();
        }

        public RelayCommand StopSearchFilesCommand { get; }
        public RelayCommand DownloadWebsiteCommand { get; }

        public RelayCommand DisplayContentCommand { get; }

        public RelayCommand SelectedPathCommand { get; }

        private void OnDownloadWebsite()
        {
            if(string.IsNullOrEmpty(TargetFolder))
            {
                MessageBox.Show("Please select the target folder to dowload the site");
            }
            else
            {
                _eventAggregator.GetEvent<DownloadFileEvent>().Publish(TargetFolder);
            }
        }

        private void OnDisplayContent()
        {
            _eventAggregator.GetEvent<DisplaySiteContentEvent>().Publish(TargetLink);
        }
        #endregion

        public string TargetLink
        {
            get => _targetLink;
            set
            {
                if (_targetLink != value)
                {
                    _targetLink = value;
                    RaisePropertyChanged(nameof(TargetLink));
                }
            }
        }

        public string TargetFolder
        {
            get => _targetFolder;
            set
            {
                if (_targetFolder != value)
                {
                    _targetFolder = value;
                    RaisePropertyChanged(nameof(TargetFolder));
                }
            }
        }
        #region Private Methods
        private string GetFolderPath()
        {
            using (FolderBrowserDialog folderDialog = new FolderBrowserDialog())
            {
                folderDialog.ShowNewFolderButton = false;
                folderDialog.RootFolder = Environment.SpecialFolder.Desktop;
                DialogResult result = folderDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    return folderDialog.SelectedPath;
                }
            }

            return string.Empty;
        }
        #endregion
    }
}
